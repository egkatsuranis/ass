angular.module('NameController', [])

  .controller('NameController', function($state, $cordovaCamera, socket, NameService) {

    var ctrl = this;
    ctrl.picture = '';

    ctrl.registerName = function(name) {
      socket.emit('registerPlayer', {name: name, picture: ctrl.picture});
    };

    ctrl.takePicture = function() {
      document.addEventListener("deviceready", function () {

          var options = {
              quality: 50,
              destinationType: Camera.DestinationType.DATA_URL,
              sourceType: Camera.PictureSourceType.CAMERA,
              allowEdit: true,
              encodingType: Camera.EncodingType.PNG,
              targetWidth: 100,
              targetHeight: 100,
              popoverOptions: CameraPopoverOptions,
              saveToPhotoAlbum: false,
              correctOrientation:true,
              cameraDirection: 1
          };

          $cordovaCamera.getPicture(options).then(function(imageData) {
              ctrl.picture = "data:image/jpeg;base64," + imageData;
              NameService.picture(ctrl.picture);
          }, function(err) {
              console.log('SNAFU');
          });

      }, false);
    };

    socket.on('registeredPlayer', function(player) {
      console.log('Registered as', player.name);
      NameService.setName(player.name);
      $state.go('join');
    });

  });

angular.module('GameService', [])

.factory('socket', function (socketFactory) {
    return socketFactory({
      ioSocket: io.connect('http://localhost:3000')
      //ioSocket: io.connect('http://ass.arcanearts.tech')
    });
  })

.factory('NameService', function($state, $window, socket) {
    $state.go('name');
    var name = '';
    var picture = '';

    return {
      getName: function() {
        return name;
      },
      setName: function(n) {
        name = n;
        $window.sessionStorage.setItem('name', name);
      },
      picture: function(img){
        if(img){
          picture = img;
        }
        return picture;
      }
    }
});

angular.module('JoinController', [])

  .controller('JoinController', function($scope, $state, socket, NameService) {
    var ctrl = this;

    ctrl.games = [];

    ctrl.name = NameService.getName();

    if (!ctrl.name) {
      $state.go('name');
    }

    ctrl.getAvailableGame = function(games) {
      return games.filter(function(game){
        return game.players.length < 3;
      });
    };

    ctrl.createGame = function(){
      console.log('creating game');
      socket.emit('createGame', {});
    };

    ctrl.joinGame = function(gameIndex){
      console.log('trying to join game %d', gameIndex);
      socket.emit('joinGame', gameIndex);
    };

    socket.on('gamesUpdated', function(games){
      console.log('games updated');
      ctrl.games = ctrl.getAvailableGame(games);
    });

    socket.emit('getGames');

    socket.on('cannotJoinGame', function() {
      console.log('cannot join game');
    });

    socket.on('joinedGame', function() {
      $state.go('game');
    });

  });

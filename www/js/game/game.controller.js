angular.module('GameController', [])

  .controller('GameController', function($state, socket, NameService, game) {
    var ctrl = this;

    ctrl.gameState = game;
    ctrl.name = NameService.getName();

    ctrl.stateIsWaiting = function() {
      return ctrl.gameState.players.length < 3;
    };

    ctrl.stateIsGameOver = function() {
      return ctrl.gameState.history.length == 5 && ctrl.gameState.history[4].roundFinished;
    };

    ctrl.stateIsYourTurn = function() {
      var player = ctrl.getCurrentPlayer();
      return player && player.name == ctrl.name;
    };

    ctrl.getOwnersScore = function() {
      var found = {};
      ctrl.gameState.players.forEach(function(player) {
        if (player.name == ctrl.name) {
          found = player.score;
        }
      });
      return found;
    };

    ctrl.goToJoinGame = function() {
      $state.go('join');
    };

    ctrl.getCurrentPlayer = function() {
      if (!ctrl.gameState.history.length) {
        return ctrl.gameState.players[0];
      }

      var roundNumber = ctrl.gameState.history.length;
      var turnsTakenInCurrentRound = ctrl.gameState.history[roundNumber - 1].turns.length;

      return ctrl.gameState.players[(turnsTakenInCurrentRound + roundNumber) % 3];
    };

    ctrl.getCurrentPPEChance = function() {
      if (ctrl.stateIsWaiting()) {
        return 0;
      }

      var roundNumber = ctrl.gameState.history.length;
      return Math.round(ctrl.gameState.history[roundNumber -1].basePpe * 100);
    };

    ctrl.getCurrentAuthorizeChance = function() {
      if (ctrl.stateIsWaiting()) {
        return 0;
      }

      var roundNumber = ctrl.gameState.history.length;
      return Math.round(ctrl.gameState.history[roundNumber -1].baseAuthorize * 100);
  };

    ctrl.takeTurn = function(action) {
      socket.emit('takeTurn', action);
    };

    ctrl.getLastResult = function() {
      if (ctrl.gameState.history.length < 1) {
        return null;
      }

      return ctrl.gameState.history[ctrl.gameState.history.length -1].result;
    };

    socket.on('updatedGame', function(game){
      console.log('updated game');
      ctrl.gameState = game;
    });
  });

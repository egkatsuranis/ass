// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('ass', ['ionic', 'btford.socket-io', 'GameService', 'NameController', 'JoinController', "GameController", 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})


  .config(function($stateProvider, $urlRouterProvider, socketFactoryProvider) {

    // Ionic uses AngularUI Router, which uses the concept of states.
    // Learn more here: https://github.com/angular-ui/ui-router.
    // Set up the various states in which the app can be.
    // Each state's controller can be found in controllers.js.
    $stateProvider

      .state('name', {
        url: '/name',
        templateUrl: 'js/name/name.template.html',
        controller: 'NameController as ctrl',
        cache: false
      })

      .state('join', {
        url: '/join',
        templateUrl: 'js/join/join.template.html',
        controller: 'JoinController as ctrl',
        cache: false
      })

      .state('game', {
        url: '/game',
        templateUrl: 'js/game/game.template.html',
        controller: 'GameController as ctrl',
        cache: false,
        resolve: {
          game: function($state, $q, socket) {
            var deferred = $q.defer();

            socket.on('updatedGame', function(game){
              console.log('resolved game');
              deferred.resolve(game);
            });

            socket.on('notInGame', function() {
              deferred.reject();
              $state.go('join');
            });

            socket.emit('getGame');

            return deferred.promise;
          }
        }
      })

    ;


    // If none of the above states are matched, use this as the fallback:
    $urlRouterProvider.otherwise('/name');

  });
